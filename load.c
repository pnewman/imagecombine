#include "image.h"
#include <stdio.h>

extern struct image_descriptor images[], images_end[];

void load(void)
{
	unsigned count = images_end - images;
	unsigned i;
	printf("count %u images:\n", count);
	for (i = 0; i < count; i++) {
		printf("image %u: starts at %p, length %x\n", i, images[i].start,
				images[i].length);
	}
}
