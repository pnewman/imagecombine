#ifndef __image_h__
#define __image_h__

#include <inttypes.h>

struct image_descriptor {
	uint32_t start;
	uint32_t length;
};

#endif // __image_h__
