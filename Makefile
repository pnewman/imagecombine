AS=	$(CROSS_COMPILE)as
CC=	$(CROSS_COMPILE)gcc
LD=	$(CROSS_COMPILE)ld
STRIP=	$(CROSS_COMPILE)strip

all: system.img

PROGRAMS?=
packages= $(addsuffix .pkg.o, $(PROGRAMS)) 

system.img: loader.o $(packages) loader.ld
	$(CC) -o $@ $^

# this rule produces a stripped image suitable for customer use
loader.o: main.o load.o
	$(LD) -o $@ -r -s $^
	#$(STRIP) --strip-unneeded $@

%.pkg.o : %
	$(CC) -c elfwrap/elfwrap.S -DINPUT_FILE=\"$<\" -o $@

clean: 
	-rm -f *.o system.img
